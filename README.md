# (HTML) Image width limit

Add the style attribute `max-width:100%;height:25em;`
to image tags which only contain the `src` and `alt` attribute.

This prevents too large images from getting cropped off on the
right (if too wide) or split up over multiple pages
(if too tall).

## Global Installation - Arch Linux

To install the script as `de-p1st-image-width-limit` run the following:

```shell
makepkg -fCcsri
```

## Manual Installation

Install the python3 dependencies, e.g. with pip:

* `beautifulsoup4` (`pacman -S python-beautifulsoup4`)
* `html5lib`       (`pacman -S python-html5lib`)

And make the python script executable:

```shell
chmod +x ./image-width-limit.py
```

## Example

Global installation:

```shell
de-p1st-image-width-limit example.html example_modified.html
```

Local installation:

```shell
python3 ./image-width-limit.py example.html example_modified.html
```
